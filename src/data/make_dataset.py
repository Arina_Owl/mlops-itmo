"""
Prepare dataset
"""
import logging
from pathlib import Path

import click
import pandas as pd
from dotenv import find_dotenv, load_dotenv

FEATURES = ['u', 'g', 'r', 'i', 'z', 'redshift']
LABEL = 'class'
CLASS2IDX = {'STAR': 0, 'GALAXY': 1, 'QSO': 2}


@click.command()
@click.argument('input_filepath', type=click.Path(exists=True))
@click.argument('output_filepaths', type=click.Path(), nargs=2)
def main(input_filepath, output_filepaths):
    """ Runs data processing scripts to turn raw data from (../raw) into
        cleaned data ready to be analyzed (saved in ../processed).
    """
    logger = logging.getLogger(__name__)
    logger.info('making final data set from raw data')

    df = pd.read_csv(input_filepath)

    labels = df['class'].map(CLASS2IDX)

    df = df[FEATURES]
    df['label'] = labels
    print(df.iloc[0])

    train_df = df.sample(frac=0.75, random_state=42)
    test_df = df.drop(train_df.index)

    mean = train_df[FEATURES].mean(axis=0)
    std = train_df[FEATURES].std(axis=0)

    train_df[FEATURES] = (train_df[FEATURES] - mean) / std
    test_df[FEATURES] = (test_df[FEATURES] - mean) / std

    train_df.to_csv(output_filepaths[0], index=False)
    test_df.to_csv(output_filepaths[1], index=False)


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s ' \
              '- %(levelname)s - %(message)s'  # pylint: disable=C0103
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())

    main()  # pylint: disable=E1120
