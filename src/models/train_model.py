"""
Train model
"""
import os
import logging
import pickle
import json
from pathlib import Path

import click
import pandas as pd
import mlflow
from mlflow.models import infer_signature
from dotenv import find_dotenv, load_dotenv
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, classification_report


load_dotenv()
remote_server_uri = os.getenv("MLFLOW_TRACKING_URI")
mlflow.set_tracking_uri(remote_server_uri)

FEATURES = ['u', 'g', 'r', 'i', 'z', 'redshift']
CLASS2IDX = {'STAR': 0, 'GALAXY': 1, 'QSO': 2}


@click.command()
@click.argument('input_filepath', type=click.Path(exists=True))
@click.argument('model_filepath')
@click.argument('metrics_filepath')
def main(input_filepath, model_filepath, metrics_filepath):
    """ Runs train scripts to run model training on
        cleaned data (saved in ../processed).
    """
    logger = logging.getLogger(__name__)
    logger.info('start training model')

    df = pd.read_csv(input_filepath)
    assert isinstance(df, pd.DataFrame), "input must be a valid dataframe"

    # enable autologging
    # mlflow.sklearn.autolog()

    # prepare training data
    x_train, x_test, y_train, y_test = \
        train_test_split(df[FEATURES], df['label'],
                         test_size=0.2, random_state=42)

    # train a model
    params = {
        "n_neighbors": 5
    }
    model = KNeighborsClassifier(**params)
    model.fit(x_train, y_train)
    predictions = model.predict(x_test)
    accuracy = accuracy_score(y_test, predictions)

    with mlflow.start_run():
        mlflow.log_params(params)
        mlflow.log_metric("accuracy", accuracy)
        mlflow.sklearn.log_model(
            sk_model=model,
            artifact_path="model",
            signature=infer_signature(x_train, model.predict(x_train)),
            input_example=x_train,
            registered_model_name="tracking_quickstart"
        )

    with open(model_filepath, 'wb') as file:
        pickle.dump(model, file)

    with open(metrics_filepath, 'w', encoding='utf-8') as file:
        score = {
            "accuracy_score": accuracy,
            "classification_report": classification_report(y_test, predictions)
        }
        json.dump(score, file, indent=4, ensure_ascii=False)


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - ' \
              '%(levelname)s - %(message)s'  # pylint: disable=C0103
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())

    main()  # pylint: disable=E1120
